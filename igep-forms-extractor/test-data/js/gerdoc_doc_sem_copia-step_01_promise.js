"use strict"



$(document).ready(function(){

    $("#sendForm").hide();

    var formData = function() {
        return {
            idTipoDocumento: $("#tpDoc").val()
            , ano: $("#exercicio").val()
            , idAssunto: $("#subject").select2("val")
            , tramites: [{
                idSetorEmitente: $("#origin").select2("val")
                , idSetorDestino: $("#destination").select2("val")
            }]
        };
    }

    // OBJECTO GERDOC
    var gerdoc = {
        createDocument: function() {

            return new Promise(function(resolve, reject) {
                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/documento"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                jQuery.ajax({
                    url: url
                    , method: "POST"
                    , data: formData()
                }).done(function(data, textStatus, jqXHR) {

                    var documentId = data.value;

                    chaveValor.getValorPromise('ID_DOCUMENTO_INICIAL')
                        .then(function(data) {
                            if(data.length == 0) {
                                console.log("1");
                                console.log(JSON.stringify(data));
                                return chaveValor.setValorPromise('ID_DOCUMENTO_INICIAL', documentId);
                            } else {
                                console.log("1'");
                                console.log(JSON.stringify(data));
                                return data;
                            }
                        }).then(function(data) {
                            console.log("2");
                            console.log(JSON.stringify(data));
                            resolve(documentId);
                        }).catch(function(error) {
                            reject(new Error("Erro ao tentar definir o documento -ID_DOCUMENTO- inicial do processo: ".replace('-ID_DOCUMENTO-', documentId) + error));
                        });
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Erro gerando documento: " + errorThrown));
                });


            });


        }
        , getDocumentData: function(idDoc) {
            return new Promise(function(resolve, reject) {
                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/documento(-ID_DOC-)"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                    .replace("-ID_DOC-", idDoc);
                jQuery.ajax({
                    url: url
                    , method: "GET"
                }).done(function(data, textStatus, jqXHR) {
                    resolve(data.value);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Erro buscando os dados do documento criado: " + errorThrown));
                });
            })
        }
        , getCurrentDate: function() {
            return new Date();
        }
        , getDocumentTypes: function() {
            var obj = this;
            return new Promise(function(resolve, reject) {
                var list = [];

                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/tipodoc?" + 
                        "$filter=somenteResposta eq null or somenteResposta eq 'N'" +
                        "&$select=idTipoDocumento, nome, tipo, abreProcesso, somenteResposta, lancaFase, faseDocumento/tipoFase" +
                        "&$expand=faseDocumento/tipoFase" +
                        "&$orderby=nome"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                jQuery.ajax({
                    url: url
                    , method: "GET"
                })
                .done(function(data, textStatus, jqXHR) {
                    obj.tpDocList = data.value;
                    data.value.forEach(function(item) {
                        list.push({
                            id: item.idTipoDocumento ? item.idTipoDocumento : ""
                            , text: item.nome ? (item.idTipoDocumento + " - " + item.nome) : ""
                        })
                    });
                    resolve(list);
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Erro buscando tipos de documento: " + errorThrown));
                });
            });

        }
        , getUserOriginSectors: function() {
            return new Promise(function(resolve, reject) {
                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/setorusuariodocumento(-USER-)?$filter=setor/ativo eq 'S'&$expand=setor&$orderby=setor/nome&$select=setor"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                    .replace("-USER-", config.user.login);

                jQuery.ajax({
                    url: url
                    , method: "GET"
                })
                .done(function(data, textStatus, jqXHR) {
                    resolve(
                        data.value.map(function(e){
                            return e.setor;
                        }).map(function(item) {
                            return {
                                id: item.idSetor ? item.idSetor : ""
                                , text: item.nome ? item.nome : ""
                            };
                        })
                    );
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Erro buscando setores para este usuario: " + errorThrown));
                });
            });

        }
        , getDestinations: function() {
            return new Promise(function(resolve, reject) {
                var list = [];
                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/setor?$filter=ativo eq 'S'&$select=idSetor, nome&$orderby=nome"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance)

                jQuery.ajax({
                    url: url
                    , method: "GET"
                })
                .done(function(data, textStatus, jqXHR) {
                    resolve(
                        data.value.filter(function(item) {
                            return item.idSetor != $("#origin").select2("val");
                        })
                        .map(function(item) {
                            return {
                                id: item.idSetor ? item.idSetor : ""
                                , text: item.nome ? (item.idSetor + " - " + item.nome) : ""
                            };
                        })
                    );
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Erro buscando setores: " + errorThrown));
                });
                return list;

            });
        }
        , getSubjects: function() {

            return new Promise(function(resolve, reject) {

                var tpDoc = $("#tpDoc").val();

                if((tpDoc !== undefined) && (tpDoc != "")) {
                    var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/assuntotipodocumento?$filter=idTipoDocumento eq -ID_TIPO_DOC- and ativo eq 'S'&$expand=assunto"
                        .replace("-TOKEN-", config.tokens.token_into)
                        .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                        .replace("-ID_TIPO_DOC-", tpDoc);

                    jQuery.ajax({
                        url: url
                        , method: "GET"
                    })
                    .done(function(data, textStatus, jqXHR) {
                        var list = [];
                        this.tpDocList = data.value;
                        resolve(
                            this.tpDocList.map(function(e){
                                return e.assunto;
                            }).map(function(item) {
                                return {
                                    id: item.idAssunto ? item.idAssunto : ""
                                    , text: item.nome ? (item.idAssunto + " - " + item.nome) : ""
                                };
                            })
                        );
                    })
                    .fail(function(jqXHR, textStatus, errorThrown) {
                        reject(new Error("Erro buscando assuntos: " + errorThrown));
                    });
                }
            });
        }
        , getPhases: function() {
            return this.getDocType().faseDocumento.map(function(el) {
                return {
                    id: el.tipoFase.idTipoFase
                    , text: el.tipoFase.idTipoFase + ' - ' + el.tipoFase.nome
                };
            });
        }
        , getDocType: function() {
            return gerdoc.tpDocList.filter(function(el) {
                return (el.idTipoDocumento == $("#tpDoc").val());
            })[0];
        }
    }
    // OBJETO GERDOC


    // USUARIO
    $("#user").val(config.user.login);

    // ORIGEM
    gerdoc.getUserOriginSectors()
        .then(function(originList){
            $("#origin").select2({
                data: originList
                , allowClear:true
            });
        })
        .catch(function(error) {
            alert(error);
        });

    // DESTINO
    $("#destination").select2({
        data: []
        , allowClear:true
    });
    $("#origin").on("change", function() {
        gerdoc.getDestinations()
            .then(function(destinationList){
            
                $("#destination").select2("val", "");

                $("#destination").select2({
                    data: destinationList
                    , allowClear:true
                });
            }).catch(function(error) {
                alert(error);
            });
    });

    // ASSUNTO
    $("#subject").select2({
        data: []
        , allowClear:true
    });

    // EXERCICIO
    $("#exercicio").val(new Date().getFullYear());

    // DATA ABERTURA
    $("#dtAbert").val(gerdoc.getCurrentDate().toLocaleString());

    // TIPO DOC
    gerdoc.getDocumentTypes()
        .then(function(list) {
            $("#tpDoc").select2({
                data: list
                , allowClear:true
            });
        })
        .catch(function(error) {
            alert(error);
        });
    // DESTINO FASE
    $(".destinationPhase").prop("disabled", true);

    // ASSUNTO
    $("#tpDoc").on("change", function() {
        gerdoc.getSubjects()
            .then(function(list) {

                $("#subject").select2({
                    data: list
                    , allowClear:true
                });
                $("#subject").select2("val", "");

                $(".destinationPhase").select2("val", "");
                if(gerdoc.getDocType().lancaFase == 'S') {
                    $(".destinationPhase").prop("disabled", false);
                    $(".destinationPhase").select2({
                        data: gerdoc.getPhases()
                        , allowClear:true
                    });
                } else {
                    $(".destinationPhase").prop("disabled", true);
                }
            });
    });

    $("#save").click(function(evt) {
        evt.preventDefault();
        var btn = $(this);
        showErrors("#fileValidationPane");

        gerdoc.createDocument()
            .then(function(data) {
                btn.hide();
                gerdoc.getDocumentData(data)
                    .then(function(data) {
                        var documentData = data[0];
                        $("#numDoc").val(documentData.IdDocumento);
                        $("#status").val(documentData.status);

                        setDataValue(
                            {
                                name: "idDocumento"
                                , processInstance: config.task.processInstance
                                , value: documentData.IdDocumento
                            }
                            , function(data) {
                                $("#confirm").show();
                            }
                        );

                        //$("form").submit();
                        alert("FIM!");
                    }).catch(function(error) {
                        alert(error);
                    });

            });

    });

    tinymce.init({
        selector:'#docText'
    });

    var exibirDocumentoAnterior = function() {

        getDataValue({
            name: "idDocumento"
            , processInstance: config.task.processInstance
        }
        , function(data) {
            if(data != "") {
                var idDocumentoAnterior = data.value;

                // DEFINIR SE E RESPOSTA
                gerdoc.newDoc = 
                
                $("#idDocumentoAnterior").html(idDocumentoAnterior);

                gerdoc.getDocumentData(
                    idDocumentoAnterior
                    , function(data) {
                        $("#conteudoDocumentoAnterior").html(data.value.texto);
                    }
                );
            } else {
                console.log("data doesn't exists");
                $("#documentoAnterior").hide();
            }
        });

    }

    exibirDocumentoAnterior();

});
