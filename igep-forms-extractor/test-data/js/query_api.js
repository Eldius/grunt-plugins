    /*!
    * config:
    * {
    *    apihost: "http://..."
    *    , token: "ABCDE"
    *    , processInstance: "123456"
    *    , param: "PARAM"
    *    , method: "methodName"
    *    , query: [
    *       'condition01'
    *       , 'condition02'
    *       , 'condition03'
    *       , 'conditionN'
    *    ]
    *    , expand: [
    *        attribute1Name
    *        , attribute2Name
    *        , attribute3Name
    *        , attributeNName
    *    ]
    * }
    * 
    * 
    */
    function queryApi(config) {
        return new Promise(function(resolve, reject) {
            var url = config.apihost 
                + config.token 
                + "/"
                + config.processInstance
                + config.method;
            if(config.param) {
                url = url + "(" + config.param + ")";
            }

            url = url + "$filter="
                + config.filter.join(" And ")
                + config.expand.join(", ");

            jQuery.ajax({
                url: url
                , method: 'GET'
            }).success(function(data) {
                resolve(data);
            }).fail(function(error) {
                reject(new Error(error));
            });
        });
    }
