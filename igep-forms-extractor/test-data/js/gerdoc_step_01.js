"use strict"



$(document).ready(function(){

    $("#sendForm").hide();

    var formData = function() {
        return {
            idTipoDocumento: $("#tpDoc").val()
            , ano: $("#exercicio").val()
            , idAssunto: $("#subject").select2("val")
            , tramites: [{
                idSetorEmitente: $("#origin").select2("val")
                , idSetorDestino: $("#destination").select2("val")
            }]
        };
    }

    // OBJECTO GERDOC
    var gerdoc = {
        createDocument: function(callback) {

            var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/documento"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance);

            jQuery.ajax({
                url: url
                , method: "POST"
                , data: formData()
            }).done(function(data, textStatus, jqXHR) {

                var documentId = data.value;

                var setDocumentoInicial = function(docId) {
                    var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                        .replace("-TOKEN-", config.tokens.token_into)
                        .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                    var postData = {
                            "chave": "ID_DOCUMENTO_INICIAL"
                            , "valor": docId
                        };

                    jQuery.ajax({
                        url: url
                        , method: "POST"
                        , data: postData
                    }).done(function(data) {

                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        
                    });

                }

                setDocumentoInicial(documentId);

                if(requestDataList.length > 1) {
                    requestDataList.slice(1).forEach(function(requestData, index) {
                        requestData.idDocPai = documentId;
                        jQuery.ajax({
                            url: url
                            , method: "POST"
                            , data: requestData
                            //, data: param
                        }).done(function(data, textStatus, jqXHR) {
                            console.log("ok");
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            console.log("Error sending the second package of documents: " + errorThrown);
                            alert("Erro executando requisição: " + errorThrown);
                        });
                    });
                }

                callback(data);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error sending the second package of documents: " + errorThrown);
                alert("Erro executando requisição: " + errorThrown);
            });



        }
        , getDocumentData(idDoc, callback) {
            var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/documento(-ID_DOC-)"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                .replace("-ID_DOC-", idDoc);
            jQuery.ajax({
                url: url
                , method: "GET"
            }).done(function(data, textStatus, jqXHR) {
                callback(data);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                alert("Erro buscando os dados do documento criado: " + errorThrown);
            });
        }
        , getCurrentDate: function() {
            return new Date();
        }
        , getDocumentTypes: function(callback) {
            var obj = this;
            var list = [];

            var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/tipodoc?" + 
                    "$filter=somenteResposta eq null or somenteResposta eq 'N'" +
                    "&$select=idTipoDocumento, nome, tipo, abreProcesso, somenteResposta, lancaFase, faseDocumento/tipoFase" +
                    "&$expand=faseDocumento/tipoFase" +
                    "&$orderby=nome"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance);

            var isAsynch = (callback !== undefined);

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: isAsynch
            })
            .done(function(data, textStatus, jqXHR) {
                obj.tpDocList = data.value;
                if(callback) {
                    callback(data);
                }
                data.value.forEach(function(item) {
                    list.push({
                        id: item.idTipoDocumento ? item.idTipoDocumento : ""
                        , text: item.nome ? (item.idTipoDocumento + " - " + item.nome) : ""
                    })
                });
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert("Erro buscando tipos de documento: " + errorThrown);
            });
            return list;
        }
        , getUserOriginSectors: function(callback) {
            var list = [];
            var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/setorusuariodocumento(-USER-)?$filter=setor/ativo eq 'S'&$expand=setor&$orderby=setor/nome&$select=setor"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                .replace("-USER-", config.user.login);
            var isAsynch = (callback !== undefined);

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: isAsynch
            })
            .done(function(data, textStatus, jqXHR) {
                if(callback) {
                    callback(data);
                }
                data.value.forEach(function(item) {
                    list.push({
                        id: item.idSetor ? item.idSetor : ""
                        , text: item.nome ? (item.idSetor + " - " + item.nome) : ""
                    })
                });
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert("Erro buscando setores para este usuario: " + errorThrown);
            });
            return list;
        }
        , getDestinations: function(callback) {
            var list = [];
            var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/setor?$filter=ativo eq 'S'&$select=idSetor, nome&$orderby=nome"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance)
            var isAsynch = (callback !== undefined);

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: isAsynch
            })
            .done(function(data, textStatus, jqXHR) {
                if(callback) {
                    callback(data);
                }
                data.value.filter(function(item) {
                    return item.idSetor != $("#origin").select2("val");
                })
                .forEach(function(item) {
                    list.push({
                        id: item.idSetor ? item.idSetor : ""
                        , text: item.nome ? (item.idSetor + " - " + item.nome) : ""
                    });
                });
            })
            .fail(function(jqXHR, textStatus, errorThrown) {
                alert("Erro buscando setores: " + errorThrown);
            });
            return list;
        }
        , getSubjects: function(callback) {
            var list = [];

            var tpDoc = $("#tpDoc").val();

            if((tpDoc !== undefined) && (tpDoc != "")) {
                var url = config.urls.api + "/into/-TOKEN-/-PROCESS_INSTANCE-/assuntotipodocumento?$filter=idTipoDocumento eq -ID_TIPO_DOC- and ativo eq 'S'&$expand=assunto"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance)
                    .replace("-ID_TIPO_DOC-", tpDoc);

                var isAsynch = (callback !== undefined);

                jQuery.ajax({
                    url: url
                    , method: "GET"
                    , asynch: isAsynch
                })
                .done(function(data, textStatus, jqXHR) {
                    if(callback) {
                        callback(data);
                    }
                    this.tpDocList = data.value;
                    this.tpDocList.forEach(function(item) {
                        list.push({
                            id: item.assunto.idAssunto ? item.assunto.idAssunto : ""
                            , text: item.assunto.nome ? (item.assunto.idAssunto + " - " + item.assunto.nome) : ""
                        })
                    });
                })
                .fail(function(jqXHR, textStatus, errorThrown) {
                    alert("Erro buscando assuntos: " + errorThrown);
                });
            }
            return list;
        }
        , getPhases: function(callback) {
            var phaseList = [];
            this.getDocType().faseDocumento.forEach(function(el) {
                phaseList.push({
                    id: el.tipoFase.idTipoFase
                    , text: el.tipoFase.idTipoFase + ' - ' + el.tipoFase.nome
                });
            });

            var result = phaseList.groupBy('id');

            return result;
        }
        , getDocType: function() {
            return gerdoc.tpDocList.filter(function(el) {
                return (el.idTipoDocumento == $("#tpDoc").val());
            })[0];
        }
    }
    // OBJETO GERDOC


    // USUARIO
    $("#user").val(config.user.login);

    // ORIGEM
    gerdoc.getUserOriginSectors(function(data){
        var originList = [];
        data.value.forEach(function (item) {
            originList.push({
                id: item.setor.idSetor
                , text: item.setor.nome
            });
        });
        $("#origin").select2({
            data: originList
            , allowClear:true
        });
    });

    // DESTINO
    $("#destination").select2({
        data: []
        , allowClear:true
    });
    $("#origin").on("change", function() {
        gerdoc.getDestinations(function(data){
            
            $("#destination").select2("val", "");

            var destinationList = [];
            data.value.filter(function(item) {
                return item.idSetor != $("#origin").select2("val");
            })
            .forEach(function(item) {
                destinationList.push({
                    id: item.idSetor ? item.idSetor : ""
                    , text: item.nome ? item.nome : ""
                });
            });
            $("#destination").select2({
                data: destinationList
                , allowClear:true
            });
        });
    });

    // ASSUNTO
    $("#subject").select2({
        data: []
        , allowClear:true
    });

    // EXERCICIO
    $("#exercicio").val(new Date().getFullYear());

    // DATA ABERTURA
    $("#dtAbert").val(gerdoc.getCurrentDate().toLocaleString());

    // TIPO DOC
    gerdoc.getDocumentTypes(function(data) {
        var list = [];
        data.value.forEach(function(item){
            list.push({
                id: item.idTipoDocumento
                , text: item.idTipoDocumento + ' - ' + item.nome
            })
        })
        $("#tpDoc").select2({
            data: list
            , allowClear:true
        });

    });
    // DESTINO FASE
    $(".destinationPhase").prop("disabled", true);

    // ASSUNTO
    $("#tpDoc").on("change", function() {
        gerdoc.getSubjects(function(data) {
            var list = [];
            data.value.forEach(function(item) {
                list.push({
                    id: item.assunto.idAssunto
                    , text: item.assunto.idAssunto + " - " + item.assunto.nome
                })
            });

            $("#subject").select2({
                data: list
                , allowClear:true
            });
            $("#subject").select2("val", "");

            $(".destinationPhase").select2("val", "");
            if(gerdoc.getDocType().lancaFase == 'S') {
                $(".destinationPhase").prop("disabled", false);
                $(".destinationPhase").select2({
                    data: gerdoc.getPhases()
                    , allowClear:true
                });
            } else {
                $(".destinationPhase").prop("disabled", true);
            }
        });
    });

    $("#save").click(function(evt) {
        evt.preventDefault();
        var btn = $(this);
        showErrors("#fileValidationPane");

        gerdoc.createDocument(function(data) {
            btn.hide();
            gerdoc.getDocumentData(data.value, function(data) {
                var documentData = data.value[0];
                $("#numDoc").val(documentData.IdDocumento);
                $("#status").val(documentData.status);

                setDataValue(
                    {
                        name: "idDocumento"
                        , processInstance: config.task.processInstance
                        , value: documentData.IdDocumento
                    }
                    , function(data) {
                        $("#confirm").show();
                    }
                );

                $("form").submit();
            });

        });

    });

    tinymce.init({
        selector:'#docText'
    });

    var exibirDocumentoAnterior = function() {

        getDataValue({
            name: "idDocumento"
            , processInstance: config.task.processInstance
        }
        , function(data) {
            if(data != "") {
                var idDocumentoAnterior = data.value;

                // DEFINIR SE E RESPOSTA
                gerdoc.newDoc = 
                
                $("#idDocumentoAnterior").html(idDocumentoAnterior);

                gerdoc.getDocumentData(
                    idDocumentoAnterior
                    , function(data) {
                        $("#conteudoDocumentoAnterior").html(data.value.texto);
                    }
                );
            } else {
                console.log("data doesn't exists");
                $("#documentoAnterior").hide();
            }
        });

    }

    exibirDocumentoAnterior();

    chaveValor.getValores(function(d) {
        console.log('Step 0:\n' + JSON.stringify(d));
    });

    chaveValor.getValoresPromise()
        .then(function(d) {
            console.log('Step 1:\n' + JSON.stringify(d));
            return Promise.all(d.value);
        })
        .then((d) => {
            console.log('Step 2:\n' + JSON.stringify(d));
        });

});
