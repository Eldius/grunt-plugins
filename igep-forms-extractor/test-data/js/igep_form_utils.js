
/*!
*  getDadosFuncionario(callback):
*    - Callback recebe os dados recebidos do serviço.
*  chaveValor:
*  * setValor(chave, valor)
*  * setValorPromise(chave, valor)
*  * getValor(chave)
*  * getValores(callback)
*  * getValoresPromise()
*  * getValoresPromise()
*  * getValoresPromise()
*  * getValoresPromise()
*  * getValoresPromise()
*/

    function getDadosFuncionario(callback) {

        jQuery.ajax({
            url:config.urls.api + '/igep/token/generic/LNOMEEMAIL/'.replace('token', config.tokens.token_into)
            , data: 'login=WSILVA'
            , method: "GET"
            , asynch: false
        }).done(function(data) {
            callback(data);
        }).fail(function() {
            callback({}, errorThrown);
        });
    }

    var chaveValor = {
        setValor: function(chave, valor) {

            var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                .replace("-TOKEN-", config.tokens.token_into)
                .replace("-PROCESS_INSTANCE-", config.task.processInstance);

            var postData = {
                    "chave": chave
                    , "valor": valor
                };

            jQuery.ajax({
                url: url
                , method: "POST"
                , data: postData
                , asynch: false
            }).done(function(data) {

            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error setting the start document: " + errorThrown);
                callback({}, errorThrown);
                alert("Erro executando uma requisição: " + errorThrown);
            });

        }
        ,
        setValorPromise: function(chave, valor) {

            return new Promise(function(resolve, reject) {
                var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                var postData = {
                        "chave": chave
                        , "valor": valor
                    };

                jQuery.ajax({
                    url: url
                    , method: "POST"
                    , data: postData
                    , asynch: false
                }).done(function(data) {
                    resolve(data);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Error setting the start document: " + errorThrown));
                });
            });


        }
        , getValor: function(chave) {

            var resultData;

            var url = "";
            if(chave) {
                url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor(-CHAVE-)"
                        .replace("-CHAVE-", chave)
                        .replace("-TOKEN-", config.tokens.token_into)
                        .replace("-PROCESS_INSTANCE-", config.task.processInstance);
            } else {
                url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                        .replace("-TOKEN-", config.tokens.token_into)
                        .replace("-PROCESS_INSTANCE-", config.task.processInstance);
            }

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: false
            }).done(function(data) {
                resultData = data;
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error setting the start document: " + errorThrown);
                callback({}, errorThrown);
                alert("Erro executando uma requisição: " + errorThrown);
            });

            return resultData;
        }
        , getValorPromise: function(chave) {

            return new Promise(function(resolve, reject) {
                var url = "";
                if(chave) {
                    url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor(-CHAVE-)"
                            .replace("-CHAVE-", chave)
                            .replace("-TOKEN-", config.tokens.token_into)
                            .replace("-PROCESS_INSTANCE-", config.task.processInstance);
                } else {
                    url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                            .replace("-TOKEN-", config.tokens.token_into)
                            .replace("-PROCESS_INSTANCE-", config.task.processInstance);
                }

                jQuery.ajax({
                    url: url
                    , method: "GET"
                    , asynch: false
                }).done(function(data) {
                    resolve(data.value);
                }).fail(function(jqXHR, textStatus, errorThrown) {
                    reject(new Error("Error setting the start document: " + errorThrown));
                });
            });

        }
        , getValores: function(callback) {

            var resultData;

            var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance);

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: false
                , success: function(data) {
                    callback(data);
                }
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error setting the start document: " + errorThrown);
                callback({}, errorThrown);
                alert("Erro executando uma requisição: " + errorThrown);
            });

            return resultData;
        }
        , getValoresPromise: function() {
            return new Promise(
                // The resolver function is called with the ability to resolve or
                // reject the promise
                function(resolve, reject) {
                    var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                            .replace("-TOKEN-", config.tokens.token_into)
                            .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                    jQuery.ajax({
                        url: url
                        , method: "GET"
                        , asynch: false
                    }).done(function(data) {
                        resolve(data);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        console.log("Error setting the start document: " + errorThrown);
                        reject(errorThrown);
                    });
                }
            );
        }
        , getObjetoValores: function(callback) {
            var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                    .replace("-TOKEN-", config.tokens.token_into)
                    .replace("-PROCESS_INSTANCE-", config.task.processInstance);

            jQuery.ajax({
                url: url
                , method: "GET"
                , asynch: false
            }).done(function(data) {
                var result = {};
                data.value.forEach(function(e) {
                    result[e.chave] = e.valor;
                });
                callback(result);
            }).fail(function(jqXHR, textStatus, errorThrown) {
                console.log("Error setting the start document: " + errorThrown);
                callback({}, errorThrown);
            });
        }
        , getObjetoValoresPromise: function() {
            return new Promise(
                // The resolver function is called with the ability to resolve or
                // reject the promise
                function(resolve, reject) {
                    var url = config.urls.api + "/igep/-TOKEN-/-PROCESS_INSTANCE-/3/chavevalor"
                            .replace("-TOKEN-", config.tokens.token_into)
                            .replace("-PROCESS_INSTANCE-", config.task.processInstance);

                    jQuery.ajax({
                        url: url
                        , method: "GET"
                        , asynch: false
                    }).done(function(data) {
                        var result = {};
                        data.value.forEach(function(e) {
                            result[e.chave] = e.valor;
                        });
                        resolve(result);
                    }).fail(function(jqXHR, textStatus, errorThrown) {
                        console.log("Error setting the start document: " + errorThrown);
                        reject(errorThrown);
                    });
                }
            );
        }
    }
