/*
 * grunt-igep-forms-extractor
 *
 *
 * Copyright (c) 2016 Eldius
 * Licensed under the MIT license.
 */

'use strict';

var Q = require("q");
var chalk = require("chalk");

var cheerio = require('cheerio');

module.exports = function(grunt) {

  // Please see the Grunt documentation for more information regarding task
  // creation: http://gruntjs.com/creating-tasks


  grunt.registerMultiTask(
    'igep_forms_extractor',
    'The best Grunt plugin ever.',
    function() {
      // Merge task-specific and/or target-specific options with these defaults.
      var options = this.options({
        punctuation: '.',
        separator: ', ',
        destFolder: 'dist'
      });

      // Iterate over all specified file groups.
      this.files.forEach(function(f) {
        console.log(chalk.blue('files group: ' + f.dest));
        // Warn on and remove invalid source files (if nonull was set).
        //grunt.log.writeln(chalk.blue('Source file "' + f.toString() + '" not found.'));
        //grunt.log.writeln(chalk.blue('param: ' + JSON.stringify(f)));
        f.src.map(function(filepath) { // html
          // Read file source.
          //grunt.log.writeln(chalk.blue('File "' + filepath + '" created.'));
          console.log(chalk.blue("parsing file '" + filepath + "'..."));
          var fileContent = grunt.file.read(filepath);
          return {
            file: filepath,
            html: cheerio.load(fileContent)('#form_delimiter'),
            content: fileContent
          };
      }).map(function(file) {
          file.head = cheerio.load(file.content)('#form_head');
          return file;
      }).map(function(file) { // script
          var html = cheerio.load(file.content);

          var script = "";
          html('script').map(function(index, el) { //
            return options.basePath + el.attribs.src;
          }).filter(function(index, path) {
            return (path.toUpperCase().search('(STATIC|VIEWHTMLFORM|HTTP|\/\/|UNDEFINED)') < 0);
          }).map(function(index, path) {
            grunt.log.writeln(chalk.blue('including script: ' + path));
            script = script + "\n\n\n/* -FILE_NAME- */\n\n\n".replace('-FILE_NAME-', path) + grunt.file.read(path);
          }); //.join(grunt.util.normalizelf(options.separator));

          file.script = script;

          return file;

        }).map(function(file) { //css

          var html = cheerio.load(file.content);

          var css = "";
          html('link').map(function(index, el) { //
            return options.basePath + el.attribs.href;
          }).filter(function(index, path) {
            return (path.toUpperCase().search('(STATIC|VIEWHTMLFORM|HTTP|\/\/|UNDEFINED)') < 0);
          }).map(function(index, path) {
            grunt.log.writeln(chalk.blue('including style: ' + path));
            css = css + "\n\n\n/* -FILE_NAME- */\n\n\n".replace('-FILE_NAME-', path) + grunt.file.read(path);
          });

          file.css = css;

          return file;
        }).map(function(file) { // write file
          // grunt.log.writeln(chalk.blue("attribute: " + file.file));

          var getFolder = function(file) {
            var fileName = file.file;
            var _folder = fileName.split("/");
            return options.destFolder + "/" + f.orig.dest + "/" + _folder[_folder.length - 1].split(".")[0];
          };
          var folderName = getFolder(file);
          grunt.log.writeln(chalk.blue('destination folder: ' + folderName));
          grunt.file.mkdir(folderName);
          grunt.file.write(folderName + "/html.html", file.html);
          grunt.file.write(folderName + "/head.html", file.head);
          grunt.file.write(folderName + "/script.js", file.script);
          grunt.file.write(folderName + "/style.css", file.css);

          return file;
        });

      });

      // Print a success message.
      grunt.log.writeln('Process finished successfully...');
    });
};
